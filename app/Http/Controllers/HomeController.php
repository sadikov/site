<?php

namespace App\Http\Controllers;

use App\Lib\JsonRpc\Client;
use \GuzzleHttp\Exception\GuzzleException;

class HomeController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        try {
            $balance = Client::get('balance', 'balance@userBalance', ['user_id' => 4]);
            $history = Client::get('balance', 'balance@history', ['limit' => 10]);
        } catch (GuzzleException $exception) {
            echo "Сервис balance временно не доступен.";
            exit;
        }
        return view('index', [
            'balance' => $balance,
            'history' => $history
        ]);
    }
}
