<?php


namespace App\Lib\JsonRpc;

class Client
{
    /**
     * @param string $endpoint
     * @param string $method
     * @param array $params
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    static public function get(string $endpoint, string $method, array $params = [])
    {
        $client = new \GuzzleHttp\Client();
        $data = [
            'id' => 1,
            'jsonrpc' => '2.0',
            'method' => $method,
            'params' => $params
        ];
        $response = $client->request('POST', env('APP_BALANCE_URL') . '/api/v1/' . $endpoint, [
            'json' => $data,
            'headers' => [
                'Content-Type' => 'application/json'
            ],
        ]);
        $response = json_decode($response->getBody()->getContents())->result;
        return $response;
    }
}
